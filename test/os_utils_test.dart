library os_utils.test;

import 'dart:io';

import 'package:os_utils/os_utils.dart';
import 'package:system_info/system_info.dart';
import 'package:test/test.dart';

void main() {
  test('user', () async {
    final user = User.current();
    expect(user, isNotNull);
    expect(User.existsSync(user.name), isTrue);
    expect(await User.exists(user.name), isTrue);
  });
}

library os_utils.user;

import 'dart:convert';
import 'dart:async';
import 'dart:io';

String _removeNewLine(String value) => value.substring(0, value.length - 1);

class User {
  User(this.id, this.name);

  factory User.fromName(String name) {
    if (Platform.isAndroid || Platform.isLinux || Platform.isMacOS) {
      final result = Process.runSync('id', ['-u', name]);
      if ('' != result.stderr) {
        throw new ArgumentError('Invalid name.');
      }
      return new User(int.parse(_removeNewLine(result.stdout)), name);
    } else {
      throw new UnimplementedError('Not implemented yet');
    }
  }

  final int id;
  final String name;

  static User current() {
    if (Platform.isAndroid || Platform.isLinux || Platform.isMacOS) {
      return new User.fromName(_removeNewLine(Process.runSync('whoami', []).stdout));
    } else {
      throw new UnimplementedError('Not implemented yet');
    }
  }

  static Future<bool> exists(String name) async {
    if (Platform.isAndroid || Platform.isLinux || Platform.isMacOS) {
      return '' == (await Process.run('id', ['-u', name])).stderr;
    } else {
      throw new UnimplementedError('Not implemented yet');
    }
  }

  static bool existsSync(String name) {
    if (Platform.isAndroid || Platform.isLinux || Platform.isMacOS) {
      return '' == Process.runSync('id', ['-u', name]).stderr;
    } else {
      throw new UnimplementedError('Not implemented yet');
    }
  }
}
